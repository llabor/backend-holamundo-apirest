package com.techu.holamundo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {

    @RequestMapping(path="/", produces="application/json")
    //@GetMapping("/")
    public String index(){
        return "Home";
    }

    @GetMapping(path="/saludos", produces="application/json")
    public String saludar(@RequestParam(value="nombre", defaultValue="Tech U!") String name){
        // public String saludar(@RequestParam String name)
        return String.format("Hola %s ;)", name);
    }
}
